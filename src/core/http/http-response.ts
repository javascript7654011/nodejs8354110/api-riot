import { Response } from 'express';
import { HttpStatus } from '../models';

export class HttpResponse {
  static ok<T>(res: Response, data: T | null = null): void {
    res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      descriptionCode: 'OK',
      data,
    });
  }

  static notAceptable(res: Response, error: string): void {
    res.status(HttpStatus.NOT_ACEPTABLE).json({
      statusCode: HttpStatus.NOT_ACEPTABLE,
      descriptionCode: 'NOT ACEPTABLE',
      error,
    });
  }

  static internalError(res: Response): void {
    res.status(HttpStatus.SERVER_INTERNAL_ERROR).json({
      statusCode: HttpStatus.SERVER_INTERNAL_ERROR,
      descriptionCode: 'INTERNAL ERROR',
    });
  }
}

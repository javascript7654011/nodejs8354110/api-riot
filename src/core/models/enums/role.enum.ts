export enum Role {
  Client = 'CLIENT',
  Aux = 'AUX',
  Admin = 'ADMIN',
}

export enum HttpStatus {
  OK = 200,
  NOT_ACEPTABLE = 406,
  SERVER_INTERNAL_ERROR = 500,
}

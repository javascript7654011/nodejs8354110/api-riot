import 'dotenv/config';

export class Settings {
  host: string;
  port: number;

  constructor() {
    this.host = String(process.env.HOST);
    this.port = Number(process.env.PORT);
  }
}

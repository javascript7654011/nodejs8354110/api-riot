import { Column, Entity } from 'typeorm';
import { Role } from '../../core/models';
import { BaseEntity } from './base-entity';

@Entity({ name: 'users' })
export class User extends BaseEntity {
  @Column({ type: 'varchar', unique: true })
  email: string;

  @Column({ type: 'varchar', unique: true })
  username: string;

  @Column({ type: 'varchar' })
  password: string;

  @Column({ type: 'varchar', default: Role.Client })
  role: Role;

  @Column({ type: 'boolean', default: false })
  isActive: boolean;
}

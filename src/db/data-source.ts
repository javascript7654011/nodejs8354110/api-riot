import { DataSource } from 'typeorm';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';

export const AppDataSource = new DataSource({
  type: 'sqlite',
  database: 'src/db/sqlite3.db',
  synchronize: true,
  logging: false,
  entities: ['src/db/entities/*.entity{.ts,.js}'],
  migrations: ['src/db/migrations/*{.ts,.js}'],
  subscribers: ['src/db/subscribers/*{.ts,.js}'],
  namingStrategy: new SnakeNamingStrategy(),
});

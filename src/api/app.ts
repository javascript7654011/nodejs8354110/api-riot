import express, { Application } from 'express';
import { IRoute } from '../core/models';
import { Settings } from '../core/settings';

export class App extends Settings {
  app: Application;

  constructor(private modules: IRoute[]) {
    super();
    this.app = express();
    this.initMiddlewares();
    this.initModules();
    this.initServer();
  }

  private initMiddlewares(): void {
    this.app.use(express.urlencoded({ extended: true }));
    this.app.use(express.json());
  }

  private initModules(): void {
    this.modules.map(({ path, router }) =>
      this.app.use('/rest-api' + path, router)
    );
  }

  private initServer(): void {
    this.app.listen(this.port, this.host, () =>
      console.log(`🚀 Server running on http://${this.host}:${this.port}/`)
    );
  }
}

import { Role } from '../enums';

export type User = {
  id: string;
  email: string;
  username: string;
  createdAt: Date;
  updatedAt: string;
  role: Role;
  isActive: boolean;
};

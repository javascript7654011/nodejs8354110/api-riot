import { Request, Response } from 'express';
import { QueryFailedError } from 'typeorm';
import { HttpResponse } from '../../../../core/http';
import { AuthService, UserService } from '../services';

export class AuthController {
  constructor(
    private userService: UserService = new UserService(),
    private authService: AuthService = new AuthService()
  ) {}

  register = async (req: Request, res: Response) => {
    const { email, username, password } = req.body;
    this.userService
      .createUser(email, username, password)
      .then(() => HttpResponse.ok(res))
      .catch((err) => {
        if (err instanceof QueryFailedError) {
          const fielDuplicate = String(err).includes('user.email')
            ? 'Email'
            : 'Username';
          return HttpResponse.notAceptable(
            res,
            `Field duplicate: ${fielDuplicate}`
          );
        }
        HttpResponse.internalError(res);
      });
  };

  login = async (req: Request, res: Response) => {
    const { email, password } = req.body;
    try {
      await this.authService.authenticate(email, password);
      HttpResponse.ok(res);
    } catch (err) {
      if (err === new Error('Failed Authenticate'))
        return HttpResponse.notAceptable(res, `Email or password wrong`);
      HttpResponse.internalError(res);
    }
  };
}

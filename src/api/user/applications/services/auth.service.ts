import { User } from '../../../../db/entities';
import { BcryptLib } from '../../../../shared/libs';
import { UserRepository } from '../../infraestructure';

export class AuthService {
  constructor(private userRepository: UserRepository = new UserRepository()) {}

  async authenticate(email: string, password: string): Promise<User> {
    const user = await this.userRepository.findOne(email);
    if (!user) throw new Error('Failed Authenticate');
    const compare = await BcryptLib.compare(password, user.password);
    if (!compare) throw new Error('Failed Authenticate');
    return user;
  }
}

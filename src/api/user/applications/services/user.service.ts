import { User } from '../../../../db';
import { BcryptLib } from '../../../../shared/libs';
import { UserRepository } from '../../infraestructure';

export class UserService {
  constructor(private userRepository: UserRepository = new UserRepository()) {}

  async createUser(
    email: string,
    username: string,
    password: string
  ): Promise<User> {
    const hash = await BcryptLib.hash(password);
    return this.userRepository.createOne(email, username, hash);
  }
}

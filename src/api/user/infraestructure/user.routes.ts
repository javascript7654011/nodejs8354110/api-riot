import { Router } from 'express';
import { IRoute } from '../../../core/models';
import { AuthController } from '../applications';

export class UserRoutes implements IRoute {
  path: string;
  router: Router;

  constructor(private authController = new AuthController()) {
    this.path = '/v1/user';
    this.router = Router();
    this.initRoutes();
  }

  private initRoutes(): void {
    this.router.post('/sign-up', this.authController.register);
  }
}

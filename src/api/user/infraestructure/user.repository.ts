import { Repository } from 'typeorm';
import { AppDataSource, User } from '../../../db';

export class UserRepository {
  private userModel: Repository<User>;

  constructor() {
    this.userModel = AppDataSource.getRepository(User);
  }

  createOne(email: string, username: string, password: string): Promise<User> {
    const user = new User();
    user.email = email;
    user.username = username;
    user.password = password;
    return this.userModel.save(user);
  }

  findOne(email: string): Promise<User | null> {
    return this.userModel.findOne({ where: { email } });
  }
}

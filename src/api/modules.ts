import { IRoute } from '../core/models';
import { UserRoutes } from './user/infraestructure';

export const modules: IRoute[] = [new UserRoutes()];

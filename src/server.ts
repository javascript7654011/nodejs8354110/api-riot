import { App } from './api/app';
import { modules } from './api/modules';
import { AppDataSource } from './db';

function bootstraper() {
  new App(modules);
  AppDataSource.initialize()
    .then(() => console.log('Base de datos inicializada'))
    .catch((error) => console.log(error));
}

bootstraper();

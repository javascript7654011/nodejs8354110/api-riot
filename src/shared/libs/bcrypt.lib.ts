import bcrypt from 'bcrypt';

export class BcryptLib {
  static async hash(data: string): Promise<string> {
    const salt = await bcrypt.genSalt(10);
    return bcrypt.hash(data, salt);
  }

  static compare(data: string, dataEncrypt: string): Promise<boolean> {
    return bcrypt.compare(data, dataEncrypt);
  }
}
